#include <stdio.h>
int inp();
int comp();
void out();

int inp()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int comp(int a,int b)
{
    int c;
    c=a+b;
    return c;
}
void out(int a,int b,int c)
{
    printf("%d + %d = %d\n",a,b,c);
}
int main()
{
    int a,b,c;
    a=inp();
    b=inp();
    c=comp(a,b);
    out(a,b,c);
    return 0;
}